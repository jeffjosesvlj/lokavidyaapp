package com.iitb.mobileict.lokavidya.ServerExplorer;


public class RowItem {

    private String video_name;
    private int pic_id;
    private String desc;
    private String time;

    public RowItem(String video_name, int pic_id, String desc,
                   String time) {

        this.video_name = video_name;
        this.pic_id =pic_id;
        this.desc = desc;
        this.time = time;
    }

    public String getMember_name() {
        return video_name;
    }

    public void setMember_name(String member_name) {
        this.video_name = member_name;
    }

    public int getProfile_pic_id() {
        return pic_id;
    }

    public void setProfile_pic_id(int profile_pic_id) {
        this.pic_id = profile_pic_id;
    }

    public String getStatus() {
        return desc;
    }

    public void setStatus(String status) {
        this.desc = status;
    }

    public String getContactType() {
        return time;
    }

    public void setContactType(String contactType) {
        this.time = contactType;
    }

}