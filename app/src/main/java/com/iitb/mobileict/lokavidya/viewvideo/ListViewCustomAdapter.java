package com.iitb.mobileict.lokavidya.viewvideo;

/**
 * Created by root on 11/5/16.
 */


        import java.util.ArrayList;

        import android.app.Activity;
        import android.content.Context;
        import android.graphics.Color;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.iitb.mobileict.lokavidya.R;

public class ListViewCustomAdapter extends BaseAdapter{

    ArrayList<Object> itemList;

    public Context context;
    public LayoutInflater inflater;

    public ListViewCustomAdapter(Context context, ArrayList<Object> itemList) {
        super();

        this.context = context;
        this.itemList = itemList;

        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public static class ViewHolder
    {
        ImageView imgViewLogo;
        TextView txtViewTitle;
        TextView txtViewDescription;
        TextView views;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder;
        if(convertView==null)
        {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.items, null);

            holder.imgViewLogo = (ImageView) convertView.findViewById(R.id.imgViewLogo);
            holder.txtViewTitle = (TextView) convertView.findViewById(R.id.txtViewTitle);
            holder.txtViewDescription = (TextView) convertView.findViewById(R.id.txtViewDescription);
            holder.views = (TextView) convertView.findViewById(R.id.views);

            convertView.setTag(holder);
        }
        else
            holder=(ViewHolder)convertView.getTag();

        ItemBean bean = (ItemBean) itemList.get(position);

        holder.imgViewLogo.setImageBitmap(bean.getImage());
        holder.txtViewTitle.setText(bean.getTitle());
        holder.txtViewTitle.setTextColor(Color.BLACK);
        holder.txtViewTitle.setAllCaps(true);
        holder.txtViewDescription.setText(bean.getDescription());
        holder.txtViewDescription.setTextColor(Color.BLACK);
        holder.views.setText("20Million");
        holder.views.setTextColor(Color.BLACK);

        return convertView;
    }

}