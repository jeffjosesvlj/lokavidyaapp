package com.iitb.mobileict.lokavidya.viewvideo;

import android.graphics.Bitmap;

/**
 * Created by root on 11/5/16.
 */
public class ItemBean
{
    String title;
    String description;
    Bitmap image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}