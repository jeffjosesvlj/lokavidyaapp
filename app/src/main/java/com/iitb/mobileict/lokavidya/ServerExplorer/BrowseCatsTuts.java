package com.iitb.mobileict.lokavidya.ServerExplorer;


        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;

        import android.app.Activity;
        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.content.res.TypedArray;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.preference.PreferenceManager;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ExpandableListView;
        import android.widget.TextView;
        import android.widget.Toast;
        import android.widget.AdapterView.OnItemClickListener;
        import android.widget.ListView;

        import com.iitb.mobileict.lokavidya.Communication.Settings;
        import com.iitb.mobileict.lokavidya.Communication.postmanCommunication;
        import com.iitb.mobileict.lokavidya.R;
        import com.iitb.mobileict.lokavidya.data.browseVideoElement;
        import com.iitb.mobileict.lokavidya.ui.VideoPlayerActivity;

        import org.json.JSONArray;
        import org.json.JSONException;

public class BrowseCatsTuts extends Activity implements OnItemClickListener {
//////////////////////////////////////////////////////////



    public static List<String> listDataHeader;
    public static HashMap<String, List<String>> listDataChild;

    public HashMap<String,String> nameToId;

    public static List<String> listLinkHeader;
    public static HashMap<String, List<String>> listLinkChild;

    public static String /* VID_JSONARRAY_URL = "http://"+ Settings.serverURL+"/api/tutorials";*/
    VID_JSONARRAY_URL;
    public static String VID_CAT_JSONARRAY_URL;
    public List<browseVideoElement> videoObjList;







   ///////////////////////////////////////////////////////////////
    int novideos=0;
    String[] member_names;
    TypedArray profile_pics;
    String[] statues;
    String[] contactType;

    List<RowCateItem> rowcateItems;
    List<RowItem> rowItems;
    ListView mylistview;
    ListView mylistview1;
    TextView nocategory,novids;
    String categoryId;
    String Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browsecatstuts);
        VID_JSONARRAY_URL = "http://ruralict.cse.iitb.ac.in/lokavidya/api/tutorials/search/findByCategory?category=";
        VID_CAT_JSONARRAY_URL = "http://ruralict.cse.iitb.ac.in/lokavidya/api/categorys/search/findChildCategories?category=";
        getActionBar().setTitle("Categories");

        categoryId=getIntent().getExtras().getString("categoryId");
        Id=getIntent().getExtras().getString("Id");
        SharedPreferences sharedPrefs;
        SharedPreferences.Editor editor;
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPrefs.edit();

        editor.putString("from",categoryId);
        VID_CAT_JSONARRAY_URL = VID_CAT_JSONARRAY_URL +categoryId;
        VID_JSONARRAY_URL=VID_JSONARRAY_URL+Id;
        rowcateItems = new ArrayList<RowCateItem>();
        rowItems = new ArrayList<RowItem>();
////////////////////////////////////////////////////////////////////////////////////////////
        //provide values
        member_names = getResources().getStringArray(R.array.names_array);

        profile_pics = getResources().obtainTypedArray(R.array.pics_array);

        statues = getResources().getStringArray(R.array.sta_array);

        contactType = getResources().getStringArray(R.array.contact_array);

////////////////////////////////////////////////////////////////////////////////////////////


    /*    for (int i = 0; i < member_names.length; i++) {
            RowItem item = new RowItem(member_names[i],
                    R.drawable.ic_video_library_black_48dp, statues[i],
                    contactType[i]);
            rowItems.add(item);
        }

        for (int i = 0; i < member_names.length; i++) {
            RowCateItem item = new RowCateItem(member_names[i],
                    profile_pics.getResourceId(i, -1), statues[i]);
            rowcateItems.add(item);
        }  */
        new viewVideosTask(BrowseCatsTuts.this).execute("OK");
      //  mylistview = (ListView) findViewById(R.id.list);
      //  mylistview1 = (ListView) findViewById(R.id.list1);
      //  CateCustomAdapter adapter1 = new CateCustomAdapter(this, rowcateItems);
       // mylistview1.setAdapter(adapter1);
      //  CustomAdapter adapter = new CustomAdapter(this, rowItems);
     //   mylistview.setAdapter(adapter);
      //  profile_pics.recycle();
      //  mylistview.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        String member_name = rowItems.get(position).getMember_name();
        Toast.makeText(getApplicationContext(), "" + member_name,
                Toast.LENGTH_SHORT).show();
    }







    private class viewVideosTask extends AsyncTask<String,Void,String> implements OnItemClickListener {

        ProgressDialog pd;
        Context context;
        JSONArray vidArray;
        JSONArray catArray;

        public viewVideosTask(Context context){
            this.context=context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* pd=new ProgressDialog(context);
            pd.setMessage(getString(R.string.stitchingProcessTitle));
            pd.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            nameToId = new HashMap<String, String>();
            Log.i("AsyncTask", "inside doinbackgrnd");
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (!sharedPreferences.getBoolean("Skip", false)) {
                vidArray = postmanCommunication.okhttpgetVideoJsonArray(VID_JSONARRAY_URL, sharedPreferences.getString("token", ""));
                Log.i("videos jsonarray", vidArray.toString());
                catArray = postmanCommunication.okhttpgetVideoJsonArray(VID_CAT_JSONARRAY_URL, sharedPreferences.getString("token", ""));
                Log.i("categ jsonarray", catArray.toString());
            } else {

                Log.i("Browsing videos", "Guest(Login has been skipped)");
                vidArray = postmanCommunication.okhttpgetGuestVideoJsonArray(VID_JSONARRAY_URL);
                //   Log.i("videos guest jsonarray", vidArray.toString());
                catArray = postmanCommunication.okhttpgetGuestVideoJsonArray(VID_CAT_JSONARRAY_URL);
                // Log.i("categ guest jsonarray", catArray.toString());
            }

            if (vidArray.toString().equals("exception") || catArray.toString().equals("exception")) {
                return "nope";
            } else {
                browseVideoElement tempVidObj = new browseVideoElement();
                videoObjList = new ArrayList<browseVideoElement>();
                int i, catId;

                try {
                    for (i = 0; i < vidArray.length(); i++) {
                        tempVidObj = new browseVideoElement();
                        tempVidObj.setVideoName(vidArray.getJSONObject(i).getString("name"));
                        Log.i("setvideo name", vidArray.getJSONObject(i).getString("name"));
                        tempVidObj.setVideoId(vidArray.getJSONObject(i).getString("id"));
                        catId = vidArray.getJSONObject(i).getJSONObject("categoryMembership").getInt("categoryId");
                        tempVidObj.setCategoryID(catId);
                        tempVidObj.setCategoryName(catArray.getJSONObject(catId - 1).getString("name"));
                        if (!vidArray.getJSONObject(i).isNull("externalVideo")) {
                            tempVidObj.setVideoUrl(vidArray.getJSONObject(i).getJSONObject("externalVideo").getString("httpurl"));
                        } else {
                            tempVidObj.setVideoUrl("no URL");
                        }

                        nameToId.put(tempVidObj.getVideoName(), tempVidObj.getVideoId());

                        videoObjList.add(tempVidObj);

                    }
                    Log.i("asynctask", "data preparation successful");
                } catch (JSONException j) {
                    j.printStackTrace();
                }

            /*Iterator<browseVideoElement> it= videoObjList.iterator();
            while(it.hasNext()){
                System.out.println("-----------------videoobj list ka attribute--------:" + it.next().getVideoName());
            }*/
             /*   for (browseVideoElement b : videoObjList) {
                    if (listDataChild.containsKey(b.getCategoryName())) {
                        Log.i("Asynctask", "category existing");

                        Log.i("video name", b.getVideoName());

                        listDataChild.get(b.getCategoryName()).add(b.getVideoName());
                        // if(b.getVideoUrl()!=null) {
                        listLinkChild.get(b.getCategoryName()).add(b.getVideoUrl());
                        //}
                        Log.i("Asynctask", "videoname mapped");

                    } else {
                        Log.i("Asynctask", "new category");

                        List<String> vidtemp = new ArrayList<String>();
                        List<String> linktemp = new ArrayList<String>();
                        Log.i("video name", b.getVideoName());
                        vidtemp.add(b.getVideoName());

                        // if(b.getVideoUrl()!=null){
                        linktemp.add(b.getVideoUrl());//}

                        listDataChild.put(b.getCategoryName(), vidtemp);
                        listLinkChild.put(b.getCategoryName(), linktemp);
                        listDataHeader.add(b.getCategoryName());
                        listLinkHeader.add(b.getCategoryName());
                        Log.i("Asynctask new catg", "new category data mapped");
                    }
                }
*/
                Log.i("Asynctask", "Phew, over!");


                return vidArray.toString();
            }
        }

        @Override
        protected void onPostExecute(String s) {

            Log.i("AsyncTask", s);
            if (s.equals("nope")) {
                Toast.makeText(context,"Something went wrong, please try again later",Toast.LENGTH_SHORT).show();
                finish();
            } else {

                try {
                    JSONArray vidArray = new JSONArray(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < catArray.length(); i++) {
                    RowCateItem item;

                    try {
                        item = new RowCateItem(catArray.getJSONObject(i).getString("name"),
                                profile_pics.getResourceId(0, -1), statues[0]);
                        rowcateItems.add(item);
                        novideos++;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }



                for (int i = 0; i < vidArray.length(); i++) {
                    RowItem item = null;
                    try {
                        item = new RowItem(vidArray.getJSONObject(i).getString("name"),
                                R.drawable.ic_video_library_black_48dp, vidArray.getJSONObject(i).getString("description"),
                                contactType[0]);

                     //   Log.i("Mesaage passed", vidArray.getJSONObject(i).getString("name"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    rowItems.add(item);
                }
                //
                nocategory = (TextView) findViewById(R.id.nocategory);
                novids = (TextView) findViewById(R.id.novideos);
              //  if(novideos==0)
                   // nocategory.setVisibility(View.VISIBLE);
                mylistview1 = (ListView) findViewById(R.id.list);
                if(rowItems.size()!=0) {
                    CustomAdapter adapter = new CustomAdapter(getApplicationContext(), rowItems);
                    mylistview1.setAdapter(adapter);
                    mylistview1.setOnItemClickListener(this);
                    mylistview1.setVisibility(View.VISIBLE);
                    nocategory.setVisibility(View.GONE);
                }
                else
                {
                    novids.setVisibility(View.VISIBLE);
                    mylistview1.setVisibility(View.GONE);
                }
                mylistview = (ListView) findViewById(R.id.list1);
                if(rowcateItems.size()!=0) {
                    CateCustomAdapter adapter1 = new CateCustomAdapter(getApplicationContext(), rowcateItems);
                    mylistview.setAdapter(adapter1);
                    mylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            // Toast.makeText(getApplicationContext(), "" +catArray.getJSONObject(position).getString("name"), Toast.LENGTH_SHORT).show();

                            Intent viewVid = new Intent(getApplicationContext(), BrowseCatsTuts.class);
                            VID_JSONARRAY_URL = "";
                            VID_CAT_JSONARRAY_URL = "";

                            try {
                                viewVid.putExtra("Id", catArray.getJSONObject(position).getString("id"));
                                viewVid.putExtra("categoryId", catArray.getJSONObject(position).getString("id"));
                                SharedPreferences sharedPrefs;
                                SharedPreferences.Editor editor;
                                sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                editor = sharedPrefs.edit();

                                editor.putString("goingto", catArray.getJSONObject(position).getString("id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            startActivity(viewVid);
                            //finish();


                        }
                    });

                }
                else
                {
                    nocategory.setVisibility(View.VISIBLE);
                    mylistview.setVisibility(View.GONE);
                }



                //  CustomAdapter adapter = new CustomAdapter(this, rowItems);
                //   mylistview.setAdapter(adapter);
                profile_pics.recycle();


             //   listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);

                // setting list adapter
            //    expListView.setAdapter(listAdapter);

                // Listview on child click listener


                // pd.dismiss();
            }
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



            String member_name = rowItems.get(position).getMember_name();
           // Toast.makeText(getApplicationContext(), "" + member_name, Toast.LENGTH_SHORT).show();

            Intent playVideo = new Intent(context, VideoPlayerActivity.class);
            Bundle bundle = new Bundle();
            try {
                bundle.putString("VIDEO_ID", vidArray.getJSONObject(position).getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                bundle.putString("ZIP_URL",vidArray.getJSONObject(position).getJSONObject("externalVideo").getString("httpurl"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            playVideo.putExtras(bundle);
            Log.d("BrowseAndViewVideos", "Calling Video Player");
            SharedPreferences sharepref = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharepref.edit();
            editor.remove("playVideoName");
            editor.remove("playVideoURL");
            editor.remove("playVideoDesc");
            try {
                editor.putString("playVideoName",vidArray.getJSONObject(position).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                editor.putString("playVideoURL", vidArray.getJSONObject(position).getJSONObject("externalVideo").getString("httpurl"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            editor.putString("playVideoDesc", ""); //TODO add description if possible

            Log.d("BrowseAndViewVideos", "Logging URL:+" + sharepref.getString("playVideoURL", "NA"));
            Log.d("BrowseAndViewVideos", "Logging URL:+" + sharepref.getString("playVideoName", "NA"));
            Log.d("BrowseAndViewVideos", "Logging URL:+" + sharepref.getString("playVideoDesc", "NA"));
            editor.commit();

            startActivity(playVideo);



        }
    }



}


















